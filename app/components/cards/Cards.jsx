import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, TouchableOpacity } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import { addCard, setCards } from './cardSlice'
import { createCard, getAllCards } from '../../config/api'
import Forms from '../form/Forms'

export default function Cards ({ list, navigation }) {
  const [anchorForm, setAnchorForm] = useState(false)
  const listId = list.id
  const { cards } = useSelector(state => state.card)
  const dispatch = useDispatch()

  const fetchCards = async listId => {
    const res = await getAllCards(listId)
    const data = await res.data
    dispatch(setCards({ listId: listId, data: data }))
  }
  useEffect(() => {
    fetchCards(listId)
  }, [listId])

  const handleAddCard = async values => {
    const { name } = values
    const res = await createCard(listId, name)
    const data = await res.data
    dispatch(addCard({ listId, data }))
    handleCancel()
  }

  const handleCancel = () => {
    setAnchorForm(false)
  }

  let cardsData = cards ? cards[listId] || [] : []

  return (
    <>
      {cardsData.map(card => {
        return (
          <TouchableOpacity
            key={card.id}
            onPress={() =>
              navigation.navigate('checklist', { card: card, list: list })
            }
          >
            <Text style={styles.text}>{card.name}</Text>
          </TouchableOpacity>
        )
      })}

      {anchorForm ? (
        <Forms
          cancel={handleCancel}
          Add={handleAddCard}
          style={{ width: '100%' }}
        />
      ) : (
        <TouchableOpacity onPress={() => setAnchorForm(true)}>
          <Text style={styles.text}>+ Add Card</Text>
        </TouchableOpacity>
      )}
    </>
  )
}

const styles = StyleSheet.create({
  text: {
    fontSize: 16,
    paddingVertical: 4,
    marginBottom: 4,
    backgroundColor: '#f0f0f0',
    borderRadius: 4,
    padding: 8
  },
  addCard: {
    fontSize: 16,
    color: '#007BFF',
    marginTop: 10
  }
})
