import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  cards: {}
}

const cardsSlice = createSlice({
  name: 'cards',
  initialState,

  reducers: {
    setCards: (state, action) => {
      const { listId, data } = action.payload
      state.cards[listId] = data
    },
    addCard: (state, action) => {
      const { listId, data } = action.payload
      state.cards[listId].push(data)
    }
  }
})

export const { setCards, addCard } = cardsSlice.actions
export default cardsSlice.reducer
