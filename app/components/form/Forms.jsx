import { Formik } from 'formik'
import React from 'react'
import { TextInput, View, Button, StyleSheet } from 'react-native'

export default function Forms ({ cancel, Add,style }) {
  return (
    <View style={[styles.list,style]}>
      <Formik
        initialValues={{ name: '' }}
        onSubmit={values => {
          Add(values)
        }}
      >
        {({ handleChange, handleSubmit, values }) => (
          <View style={styles.listTitle}>
            <TextInput
              style={styles.input}
              onChangeText={handleChange('name')}
              value={values.name}
              placeholder='name'
            />
            <View style={styles.buttons}>
              <View style={styles.btnWrapper}>
                <Button onPress={() => cancel()} title='Cancel' />
              </View>
              <View>
                <Button onPress={handleSubmit} title='Add' />
              </View>
            </View>
          </View>
        )}
      </Formik>
    </View>
  )
}

const styles = StyleSheet.create({
  list: {
    backgroundColor: '#fff',
    borderRadius: 8,
    padding: 10,
    marginRight: 10,
    width: 280,
    height: 'auto',
    elevation: 3
  },
  listTitle: {
    fontWeight: 'bold',
    fontSize: 18,
    marginLeft: 5,
    marginBottom: 10
  },
  
  input: {
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    marginBottom: 10,
    paddingHorizontal: 10
  },

  buttons: {
    flexDirection: 'row',
    justifyContent: 'flex-end'
  },
  btnWrapper: {
    marginRight: 10
  }
})
