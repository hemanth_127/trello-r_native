import React from 'react'
import { Formik } from 'formik'
import { Button, StyleSheet, TextInput, View } from 'react-native'

const Form = ({ route, navigation }) => {
  const { getValues,title } = route?.params

  return (
    <Formik
      initialValues={{ name: '' }}
      onSubmit={values => {
        getValues(values)
        navigation.goBack()
      }}
    >
      {({ handleChange, handleSubmit, values }) => (
        <View style={styles.container}>
          <TextInput
            style={styles.input}
            onChangeText={handleChange('name')}
            value={values.name}
            placeholder='name'
          />
          <Button onPress={handleSubmit} title={title} />
        </View>
      )}
    </Formik>
  )
}
const styles = StyleSheet.create({
  container: {
    padding: 20
  },
  input: {
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    marginBottom: 10,
    paddingHorizontal: 10
  }
})
export default Form
