import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'

import { Boards } from '../boards/Boards'
import { List } from '../lists/Lists'
import Form from '../form/Form'
import Modal from '../checkList/Checklist'

const Stack = createStackNavigator()

export const BoardStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Group
        screenOptions={{
          headerStyle: {
            backgroundColor: 'dodgerblue'
          },
          headerTintColor: '#fff'
        }}
      >
        <Stack.Screen name='Board' component={Boards} />
        <Stack.Screen
          name='List'
          component={List}
          options={({ route }) => ({
            title: `${route.params?.name}`
          })}
        />
        <Stack.Screen
          name='CreateBoard'
          component={Form}
          title='Create Board'
        />
      </Stack.Group>

      <Stack.Group screenOptions={{ presentation: 'modal' }}>
        <Stack.Screen
          name='checklist'
          options={({ route }) => ({
            title: `${route.params?.card?.name}`
          })}
          component={Modal}
        />
      </Stack.Group>
    </Stack.Navigator>
  )
}
