import React from 'react'
import { createDrawerNavigator } from '@react-navigation/drawer'

import { BoardStack } from './BoardStack'

const Drawer = createDrawerNavigator()

export const MyDrawer = () => {
  return (
    <Drawer.Navigator>
      <Drawer.Screen
        name='Boards'
        component={BoardStack}
        options={{ headerShown: false }}
      />
    </Drawer.Navigator>
  )
}
