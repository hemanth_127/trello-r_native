import { configureStore } from '@reduxjs/toolkit'
import boardReducer from '../boards/boardSlice'
import listReducer from '../lists/listSlice'
import cardSlice from '../cards/cardSlice'
import checklistSlicer from '../checkList/checklistSlicer'
import checkItemSlicer from '../checkItems/checkItemSlicer'

export const store = configureStore({
  reducer: {
    board: boardReducer,
    list: listReducer,
    card: cardSlice,
    checkList: checklistSlicer,
    checkItem: checkItemSlicer
  }
})

export default store
