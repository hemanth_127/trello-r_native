import { CheckBox } from '@rneui/themed'
import { StyleSheet, Text, View } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import { getCheckItems, strikeCheckItems } from '../../config/api'
import { setCheckItem, toggleState } from './checkItemSlicer'
import { useEffect } from 'react'

export const CheckItem = ({ cardId, checkListId }) => {
  const { checkItems } = useSelector(state => state.checkItem)
  const dispatch = useDispatch()

  const fetchCheckItem = async checkListId => {
    const res = await getCheckItems(checkListId)
    const data = await res.data
    dispatch(setCheckItem({ id: checkListId, data: data }))
  }

  useEffect(() => {
    fetchCheckItem(checkListId)
  }, [checkListId])

  const toggleCheckedStateItem = async (checkItemId, currentState) => {
    const newState = currentState === 'complete' ? 'incomplete' : 'complete'
    const data = await strikeCheckItems(
      cardId,
      checkListId,
      checkItemId,
      newState
    )
    dispatch(toggleState({ checkListId, checkItemId, data: data.data }))
  }

  let itemData = checkItems ? checkItems[checkListId] || [] : []

  return (
    <>
      {itemData.map(ele => {
        return (
          <View style={styles.subItems} key={ele.id}>
            <CheckBox
              checked={ele.state === 'complete'}
              onPress={() => toggleCheckedStateItem(ele.id, ele.state)}
              iconType='material-community'
              checkedIcon='checkbox-outline'
              uncheckedIcon={'checkbox-blank-outline'}
            />
            <Text style={styles.text}> {ele.name} </Text>
          </View>
        )
      })}
    </>
  )
}

const styles = StyleSheet.create({
  subItems: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#fff'
  }
})
