import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  checkItems: {}
}

const checkItemSlice = createSlice({
  name: 'checkItem',
  initialState,
  reducers: {
    setCheckItem: (state, action) => {
      const { id, data } = action.payload
      state.checkItems[id] = data
    },
    addCheckItem: (state, action) => {
      const { id, data } = action.payload
      state?.checkItems[id]?.push(data)
    },
    toggleState: (state, action) => {
      const { checkListId, checkItemId, data } = action.payload
      state.checkItems[checkListId] = state.checkItems[checkListId].map(ele => {
        if (ele.id === checkItemId) {
          return data
        } else {
          return ele
        }
      })
    }
  }
})
export default checkItemSlice.reducer
export const { setCheckItem, addCheckItem, toggleState } =
  checkItemSlice.actions
