import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  lists: []
}

const listsSlice = createSlice({
  name: 'lists',
  initialState,
  reducers: {
    setLists: (state, action) => {
      state.lists = action.payload
    },
    addList: (state, action) => {
      state.lists.push(action.payload)
    }
  }
})

export default listsSlice.reducer
export const { setLists,addList } = listsSlice.actions
