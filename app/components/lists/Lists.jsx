import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import { ScrollView } from 'react-native'

import { addList, setLists } from './listSlice'
import { createList, getAllLists } from '../../config/api'
import Cards from '../cards/Cards'
import Forms from '../form/Forms'

export const List = ({ route, navigation }) => {
  const [anchorForm, setAnchorForm] = useState(false)
  const { lists } = useSelector(state => state.list)
  const dispatch = useDispatch()
  const { id } = route.params
  

  const fetchListData = async id => {
    const res = await getAllLists(id)
    const data = await res.data
    dispatch(setLists(data))
  }

  useEffect(() => {
    fetchListData(id)
  }, [id])

  const handleAddList = async values => {
    const { name } = values
    console.log(values)
    const res = await createList(id, name)
    dispatch(addList(res.data))
    handleCancel()
  }

  const handleCancel = () => {
    setAnchorForm(false)
  }

  return (
    <ScrollView style={styles.listsContainer} horizontal>
      {lists.map(list => {
        return (
          <View style={styles.list} key={list.id}>
            <Text style={styles.listTitle}>{list.name}</Text>
            <Cards list={list} navigation={navigation} />
          </View>
        )
      })}
      {anchorForm ? (
        <Forms cancel={handleCancel} Add={handleAddList} />
      ) : (
        <TouchableOpacity
          style={styles.list}
          onPress={() => setAnchorForm(true)}
        >
          <Text style={styles.listTitle}> + Add List</Text>
        </TouchableOpacity>
      )}
    </ScrollView>
  )
}

const styles = StyleSheet.create({
  listsContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    padding: 10,
    margin: 10
  },
  list: {
    backgroundColor: '#fff',
    borderRadius: 8,
    padding: 10,
    marginRight: 10,
    width: 280,
    height: 'auto',
    elevation: 3
  },
  listTitle: {
    fontWeight: 'bold',
    fontSize: 18,
    marginLeft: 5,
    marginBottom: 10
  }
})
