import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  checkList: {}
}

const checkListSlice = createSlice({
  name: 'checkList',
  initialState,
  reducers: {
    setCheckList: (state, action) => {
      const { id, data } = action.payload
      state.checkList[id] = data
    },
    createCheckList: (state, action) => {
      const { id, data } = action.payload
      state.checkList[id].push(data)
    }
  }
})

export default checkListSlice.reducer
export const { setCheckList, createCheckList } = checkListSlice.actions
