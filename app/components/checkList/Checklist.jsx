import React, { useEffect, useState } from 'react'
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ScrollView,
  TouchableWithoutFeedback
} from 'react-native'
import { MaterialIcons, AntDesign } from '@expo/vector-icons'
import { createCheckListById, getCheckList } from '../../config/api'
import { useDispatch, useSelector } from 'react-redux'
import { createCheckList, setCheckList } from './checklistSlicer'
import { CheckItem } from '../checkItems/CheckItem'

export default function Modal ({ route }) {
  const { checkList } = useSelector(state => state.checkList)
  const dispatch = useDispatch()
  const { card, list } = route.params
  const { id } = card

  const fetchChecklist = async id => {
    const res = await getCheckList(id)
    const data = await res.data
    dispatch(setCheckList({ id, data }))
  }

  useEffect(() => {
    fetchChecklist(id)
  }, [id])

  const addCheckList = async (name = 'checklist') => {
    console.log(id, name)
    const res = await createCheckListById(id, name)
    const data = await res.data
    dispatch(createCheckList({ id, data }))
  }

  let checkListData = checkList ? checkList[id] : []

  return (
    <ScrollView style={styles.container}>
      <TouchableOpacity style={styles.checklist} onPress={() => addCheckList()}>
        <MaterialIcons name='checklist' size={24} color='black' />
        <Text style={styles.header}>Checklist</Text>
      </TouchableOpacity>
      {checkListData &&
        checkListData.map(ele => {
          return (
            <View key={ele.id}>
              <TouchableWithoutFeedback
                style={styles.item}
                onPress={() => console.log('chekitem cliked', ele.name)}
              >
                <View style={styles.item}>
                  <Text style={styles.text}> {ele.name} </Text>
                  <AntDesign name='down' size={24} color='black' />
                </View>
              </TouchableWithoutFeedback>
              <CheckItem cardId={id} checkListId={ele.id} />
            </View>
          )
        })}
    </ScrollView>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 16
  },
  checklist: {
    marginLeft: 16,
    flexDirection: 'row',
    alignItems: 'center',
    width: 150,
    borderWidth: 1,
    borderColor: 'gray',
    padding: 3,
    borderRadius: 3
  },
  header: {
    fontSize: 18,
    width: '100%'
  },
  text: {
    fontSize: 18
  },
  item: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: 15,
    padding: 10,
    paddingLeft:17,
    elevation: 3,
    backgroundColor: '#fff'
  }
})
