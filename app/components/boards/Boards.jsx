import React, { useEffect, useState } from 'react'
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import { MaterialCommunityIcons } from '@expo/vector-icons'

import getAllBoards, { createBoardByName } from '../../config/api'
import { addBoard, setBoards } from './boardSlice'

export const Boards = ({ navigation }) => {
  const { boards } = useSelector(state => state.board)
  const dispatch = useDispatch()

  const fetchBoards = async () => {
    const response = await getAllBoards()
    dispatch(setBoards(response.data))
  }

  useEffect(() => {
    fetchBoards()
  }, [])

  const createBoard = async values => {
    const { name } = values
    const response = await createBoardByName(name)
    dispatch(addBoard(response.data))
  }

  return (
    <View style={styles.container}>
      {boards.map(board => {
        return (
          <TouchableOpacity
            key={board.id}
            style={styles.board}
            onPress={() => navigation.navigate('List', board)}
          >
            <Image
              style={styles.image}
              source={
                board.prefs?.backgroundImage
                  ? { uri: board.prefs.backgroundImage }
                  : require('../../assets/backimg.jpg')
              }
            />
            <Text style={styles.title}>{board.name}</Text>
          </TouchableOpacity>
        )
      })}

      <View style={styles.bottomContainer}>
        <TouchableOpacity
          style={styles.button}
          onPress={() => {
            navigation.navigate('CreateBoard', { getValues: createBoard ,title:"Create Board"})
          }}
        >
          <MaterialCommunityIcons
            name='plus-circle-outline'
            size={60}
            color='black'
          />
        </TouchableOpacity>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 10
  },
  board: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 1,
    margin: 4
  },
  image: {
    height: 50,
    width: 65,
    borderRadius: 8,
    elevation: 10
  },
  title: {
    width: '100%',
    paddingLeft: 10,
    fontSize: 18
  },
  bottomContainer: {
    position: 'absolute',
    bottom: 10,
    right: 0
  }
})
