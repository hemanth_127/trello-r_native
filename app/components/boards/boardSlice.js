import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  boards: []
}

const boardSlice = createSlice({
  name: 'board',
  initialState,
  reducers: {
    setBoards: (state, action) => {
      state.boards = action.payload
    },
    addBoard: (state, action) => {
      state.boards.push(action.payload)
    }
  }
})

export const { setBoards, addBoard } = boardSlice.actions
export default boardSlice.reducer
