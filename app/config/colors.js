export default {
  primary: 'dorgerblue',
  blue: '#2364DD',
  white: '#FFFFFF',
  box: '0 1px 1px 0 rgba(0, 0, 0, .2)'
}
