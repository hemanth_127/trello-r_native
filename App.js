import { NavigationContainer } from '@react-navigation/native'
import { Provider } from 'react-redux'

import { MyDrawer } from './app/components/routes/MyDrawer'
import store from './app/components/store/store'

export default function App () {
  return (
    <NavigationContainer>
      <Provider store={store}>
        <MyDrawer />
      </Provider>
    </NavigationContainer>
  )
}
